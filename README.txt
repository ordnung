Ordnung 0.1
===========

Ordnung is a tool to automatically sort, organize and rename your music
collection based on their tags like album, artist or title.

Copyright 2009, Erik Hahn <erik_hahn@gmx.de>

Project Homepage: https://sourceforge.net/projects/ordnung/

Usage
-----
Right now, Ordnung can only be controlled via the command line. Basic
usage is:

 ordnung <directory> <pattern>

where <directory> is where your collection is stored right now.
<pattern> is any path, which contains zero or more field references
which are enclosed by percent marks. If, for example, you want to sort
all your music located directly in C:\Music (not in any subfolders) by
Artist, then Album and have the title as name, you'd use

 ordnung "C:\Music" "C:\Music\%artist%\%album%\%title%"

to preview, and, if the displayed result is what you want,

 ordnung -m "C:\Music" "C:\Music\%artist%\%album%\%title%"

to actually move and rename the files. Note that the file extension must
not be added manually to the pattern.

See ordnung --help for more options.

Valid field references
----------------------
These field references are currently supported:

%title%     %artist%    %album artist%    %album%
%composer%  %genre%     %date%

If %album artist% would be empty otherwise, %artist% is used in its
place.

Dependencies
------------
Ordnung has been created using Python 2.6. The following 3rd party
modules must be installed as well:

    Mutagen
        <http://code.google.com/p/mutagen/>
    Scott Paul Robertson's tag_wrapper 
        <http://git.scottr.org/tag_wrapper.git/>.
