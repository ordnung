#  Copyright 2009, Erik Hahn

#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import warnings
warnings.filterwarnings("ignore", message="the md5 module is deprecated; " + \
        "use hashlib instead")

from path import path
import fnmatch
import os
import shutil
import sys

from optparse import OptionParser, OptionGroup
from tag_wrapper import tag, TagException

# Some extensions might be missing
ACCEPTEXTS = [
        ".asf", ".wma", ".wmv",     # ASF, Windows Media
        ".flac",                    # FLAC
        ".mp3",                     # MP3
        # Which extensions for Monkeysaudio?
        ".mp4", ".m4a",
        ".mpc", ".mpp", ".mp+",     # Musepack (APEv2)
        ".oga", ".ogg",             # Ogg Vorbis
        ".tta",                     # True Audio (hopefully)
        ".wv", ".wvc"               # WavPack
        ]

def issupportedfile(name):
    for ext in ACCEPTEXTS:
        if os.path.splitext(name)[1] == ext:
            return True
    return False


def mkdir(newdir):
    if not os.path.isdir(newdir):
        os.makedirs(newdir)


def newpath(file, pattern):
    # Create the tokens dictionary
    tokens = {}
    tags = tag(file)
    # TODO: add tracknumber, disknumber and possibily compilation
    for t in ["title", "artist", "album artist", "album", "composer", "genre", "date"]:
        try:
            tokens[t] = tags[t]
        except KeyError:
            tokens[t] = None
    # %album artist% is %artist% if nothing can be found in the tags
    if tokens["album artist"] is None:
        tokens["album artist"] = tokens["artist"]

    # Now replace all tokens by their values
    for i in tokens:
        repl = "%" + i + "%"
        if tokens[i] is not None:
            if Options["windows"]:
                val = sanitize_path(tokens[i][0])
            else:
                val = tokens[i][0]
            pattern = pattern.replace(repl, val)

    # Add the extension and return the new path
    return pattern + os.path.splitext(file)[1]


def safeprint(string):
    """
    Print string first trying to normally encode it, sending it to the
    console in "raw" UTF-8 if it fails.

    This is a workaround for Windows's broken console
    """
    try:
        print string
    except UnicodeEncodeError:
        print string.encode("utf-8")


def sanitize_path(string):
    # replaces all characters invalid in windows path and file names by '+'
    for i in ['/', '\\', ':', '*', '?', '"', '<', '>', '|']:
        string = string.replace(i, '+')
    return string


def files_to_move(base_dir, pattern):
    # Figure out which files to move where
    basepath = path(base_dir)
    if Options["recursive"]:
        files = basepath.walkfiles()
    else:
        files = basepath.files()

    files_return = []

    for file in files:
        if issupportedfile(file):
            try:
                t = [ file, newpath(file, pattern) ]
            except TagException:
                print "Error reading tags from " + file
            else:
                files_return.append(t)

    return files_return

Options = {}

def main():
    # Pseudo-enum for actions
    COPY = 0
    MOVE = 1
    PREVIEW = 2
    NOTHING = 3

    # Handle arguments
    usage = "Usage: %prog [options] directory pattern"
    version = "Ordnung 0.1 RC 1"
    parser = OptionParser(usage=usage, version=version)
    parser.add_option("-r", "--recursive", action="store_true",
            dest="recursive", help="also move/copy files from sub-directories",
            default=False)
    parser.add_option("-w", "--windows", action="store_true", dest="windows",
            help="generate Windows-compatible file and path names",
            default=False)

    actions = OptionGroup(parser, "Possible actions")
    actions.add_option("--preview", "-p", action="store_const",
            const=PREVIEW, dest="action", help="preview, don't make any changes (default)")
    actions.add_option("--copy", "-c", action="store_const",
            const=COPY, dest="action", help="copy files")
    actions.add_option("--move", "-m", action="store_const",
            const=MOVE, dest="action", help="move files")
    actions.add_option("--nothing", "-n", action="store_const",
            const=MOVE, dest="action", help="do nothing (for debugging)")
    parser.add_option_group(actions)
    (options, args) = parser.parse_args()
   
    # Passing these values in chains of arguments would be error-prone
    Options["recursive"] = options.recursive
    # What about Win 9x and possibily other platforms?
    if os.name == "nt":
        Options["windows"] = True
    else:
        Options["windows"] = options.windows

    try:
        base = args[0]
    except IndexError:
        print "You must specify a directory"
        sys.exit()

    try:
        pattern = args[1]
    except IndexError:
        print "You must specify a pattern"
        sys.exit()
    
    # Do stuff
    for i in files_to_move(base_dir=base, pattern=pattern):
        if Options["windows"]:
            # Windows doesn't accept trailing dots in filenames
            t = i[1].split(os.sep)
            if os.name == "nt":
                # Am I working around a bug or a feature?
                tpath = t[0] + '\\'
            else:
                tpath = t[0]
            for j in t[1:-1]:
                tpath = os.path.join(tpath, (j.rstrip('.')))
            tfile = os.path.join(tpath, t[-1])
        else:
            tpath = os.path.split(i[1])[0]
            tfile = i[1]

        if options.action == MOVE:
            mkdir(tpath)
            try:
                shutil.move(i[0], tfile)
            except shutil.Error:
                pass
        elif options.action == COPY:
            mkdir(tpath)
            try:
                shutil.copy2(i[0], tfile)
            except shutil.Error:
                pass
        elif options.action == PREVIEW:
            safeprint (i[0] + " --> " + tfile)
        else: # options.action == NOTHING
            pass


if __name__ == "__main__":
    main()
