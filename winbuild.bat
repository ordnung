@echo off
setlocal

set sevenzip="%programfiles%\7-Zip\7z.exe"
set python="%programfiles%\python26\python.exe"
set pyinstaller="%programfiles%\pyinstaller\build.py"

%python% %pyinstaller%\build.py -OO ordnung.spec
cd dist
%sevenzip% a ordnung.zip ordnung.exe ..\README.txt ..\COPYING.txt
