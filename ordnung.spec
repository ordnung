# -*- mode: python -*-
a = Analysis([os.path.join(HOMEPATH,'support\\_mountzlib.py'), os.path.join(HOMEPATH,'support\\useUnicode.py'), 'ordnung.py'],
             pathex=['D:\\tmp\\ordnung'])
pyz = PYZ(a.pure)
exe = EXE( pyz,
          a.scripts + [('O', '', 'OPTION')],
          a.binaries,
          a.zipfiles,
          a.datas,
          name=os.path.join('dist', 'ordnung.exe'),
          debug=False,
          strip=True,
          upx=True,
          console=True )
